#include "include/path_tracer.h"
#include "src/BitMap.h"
#include "src/App.h"
#include "src/CLManager.h"

//TODO:
// 0. Think of conversions from glm::vec2,3,4 to float2, 3, 4. Can I overload operators?
// 1. Directional light and other stuff.
// 2. Think of nudging camera rays in kernel. Anti aliasing stuff...
// 3. Find out cheaper way to generate random vector in a unit sphere.
// 4. So, materials?
// 5. So, different shapes?
// 6. Models?

std::vector<uint64_t> createSeeds(cl_uint pixelsNum);
std::vector<cl_float4> createRandomVectors(cl_uint pixelsNum);
std::vector<Primitive> initPrimitives();
Mat4 createMat4(glm::vec4 x_axis, glm::vec4 y_axis, glm::vec4 z_axis, glm::vec4 w_axis);
glm::mat4 lookAt(glm::vec3 eye, glm::vec3 target, glm::vec3 up);
Camera createCamera(cl_float fov);

int main(int, char **) {
	size_t global = WINDOW_WIDTH * WINDOW_HEIGHT;

	CLManager clManager = CLManager();
	cl_program program = clManager.createProgram("src/kernels/path-tracing.cl");
	cl_kernel kernel = clManager.createKernel(program, "path_tracing");

	App app("OHAYO", 1000, 80, WINDOW_WIDTH, WINDOW_HEIGHT);
	float_t tick;

	cl_mem outputBuffer_bitmap;
	cl_mem inputBuffer_state;
	cl_mem inputBuffer_camera;
	cl_mem inputBuffer_primitives;
	cl_mem inputBuffer_seeds;
	cl_int input_primitivesNum;

	std::vector<Primitive> list = initPrimitives();
	input_primitivesNum = list.size();

	Camera camera = createCamera(90.0);

	BitMap bitMap(WINDOW_WIDTH, WINDOW_HEIGHT);
	bitMap.clear();

	State state;
	state.width = WINDOW_WIDTH;
	state.height = WINDOW_HEIGHT;
	state.time = 0.0f;

	std::vector<uint64_t> seeds;
	// ---------------------------------- initializing things ------------------------------------------

	outputBuffer_bitmap = clCreateBuffer(
			clManager.context,
			CL_MEM_WRITE_ONLY,
			sizeof(Uint32) * bitMap.getPixelsNum(),
			NULL,
			&clManager.err
	);

	inputBuffer_state = clCreateBuffer(
			clManager.context,
			CL_MEM_READ_ONLY,
			sizeof(State),
			NULL,
			&clManager.err
	);

	inputBuffer_camera = clCreateBuffer(
			clManager.context,
			CL_MEM_READ_ONLY,
			sizeof(Camera),
			NULL,
			&clManager.err
	);

	inputBuffer_seeds = clCreateBuffer(
			clManager.context,
			CL_MEM_READ_ONLY,
			sizeof(uint64_t) * WINDOW_WIDTH * WINDOW_HEIGHT,
			NULL,
			&clManager.err
	);

	inputBuffer_primitives = clCreateBuffer(
			clManager.context,
			CL_MEM_READ_ONLY,
			sizeof(Primitive) * list.size(),
			NULL,
			&clManager.err
	);

	while (!app.isClosed()) {
		clManager.err = clEnqueueWriteBuffer(
				clManager.commandQueue,
				inputBuffer_state,
				CL_FALSE,
				0,
				sizeof(State),
				&state,
				0,
				nullptr,
				nullptr
		);
		clManager.checkError("Couldn't write state buffer", __FILE__, __LINE__);

		clManager.err = clEnqueueWriteBuffer(
				clManager.commandQueue,
				inputBuffer_camera,
				CL_FALSE,
				0,
				sizeof(Camera),
				&camera,
				0,
				nullptr,
				nullptr
		);
		clManager.checkError("Couldn't write camera buffer", __FILE__, __LINE__);

		clManager.err = clEnqueueWriteBuffer(
				clManager.commandQueue,
				inputBuffer_primitives,
				CL_FALSE,
				0,
				sizeof(Primitive) * list.size(),
				&list[0],
				0,
				nullptr,
				nullptr
		);
		clManager.checkError("Couldn't write primitives buffer", __FILE__, __LINE__);

		tick = SDL_GetTicks();

		state.time = tick * 0.001;
		clManager.err = clEnqueueWriteBuffer(
				clManager.commandQueue,
				inputBuffer_state,
				CL_FALSE,
				0,
				sizeof(State),
				&state,
				0,
				nullptr,
				nullptr
		);
		clManager.checkError("Couldn't write state buffer", __FILE__, __LINE__);

		seeds = createSeeds(WINDOW_WIDTH * WINDOW_HEIGHT);

		clManager.err = clEnqueueWriteBuffer(
				clManager.commandQueue,
				inputBuffer_seeds,
				CL_FALSE,
				0,
				sizeof(uint64_t) * seeds.size(),
				&seeds[0],
				0,
				nullptr,
				nullptr
		);
		clManager.checkError("Couldn't write seeds buffer", __FILE__, __LINE__);

		clManager.err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &outputBuffer_bitmap);
		clManager.err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &inputBuffer_state);
		clManager.err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &inputBuffer_camera);
		clManager.err |= clSetKernelArg(kernel, 3, sizeof(cl_mem), &inputBuffer_seeds);
		clManager.err |= clSetKernelArg(kernel, 4, sizeof(cl_mem), &inputBuffer_primitives);
		clManager.err |= clSetKernelArg(kernel, 5, sizeof(cl_int), &input_primitivesNum);
		clManager.checkError("Setting kernel arguments", __FILE__, __LINE__);

		clManager.err = clEnqueueNDRangeKernel(
				clManager.commandQueue,
				kernel,
				1, NULL, &global, NULL, 0, NULL, NULL
		);
		clManager.checkError("Enqueueing kernel", __FILE__, __LINE__);

		// Wait for the commandQueue to complete before stopping the timer
		clManager.err = clFinish(clManager.commandQueue);
		clManager.checkError("Waiting for kernel to finish", __FILE__, __LINE__);

		clManager.err = clEnqueueReadBuffer(
				clManager.commandQueue,
				outputBuffer_bitmap,
				CL_TRUE,
				0,
				sizeof(Uint32) * WINDOW_WIDTH * WINDOW_HEIGHT,
				bitMap.data,
				0,
				NULL,
				NULL
		);
		clManager.checkError("Couldn't clEnqueueReadBuffer", __FILE__, __LINE__);


		app.renderer->update(&bitMap);
		app.renderer->draw();

		app.window->readKeyboardInput();
	}

	return 0;
}

std::vector<uint64_t> createSeeds(cl_uint pixelsNum) {
	std::mt19937_64 gen;
	std::random_device rd;

	std::vector<uint64_t> seeds;
	seeds.resize(pixelsNum);

	gen.seed(rd());

	std::uniform_int_distribution<uint64_t> randomInt;

	for (uint64_t &seed : seeds) {
		seed = randomInt(gen);
	}

	return seeds;
}

std::vector<cl_float4> createRandomVectors(cl_uint pixelsNum) {
	std::mt19937_64 gen;
	std::random_device rd;

	std::vector<cl_float4> seeds;
	seeds.resize(pixelsNum);

	gen.seed(rd());

	std::uniform_real_distribution<cl_float> rndFloat;

	for (cl_float4 &seed : seeds) {
		cl_float x = rndFloat(gen);
		cl_float y = rndFloat(gen);
		cl_float z = rndFloat(gen);

		cl_float u = rndFloat(gen);

		glm::vec3 v = glm::normalize(glm::vec3(x, y, z));

		u = std::cbrt(u);

		seed = createVector4<cl_float4, cl_float>(v.x * u, v.y * u, v.z * u, 0.0f);
	}

	return seeds;
}

std::vector<Primitive> initPrimitives() {
	Primitive primitive;
	std::vector<Primitive> list;

	primitive.position = createVector4<cl_float4, cl_float>(0.0f, 0.0f, -0.5f, 1.0f);
	primitive.color = createVector4<cl_float4, cl_float>(0.8f, 0.3f, 0.3f, 0.0f);
	primitive.radius = 0.2f;
	primitive.material = MATERIAL_METAL;

	list.push_back(primitive);

	primitive.position = createVector4<cl_float4, cl_float>(0.5f, 0.0f, -1.3f, 1.0f);
	primitive.color = createVector4<cl_float4, cl_float>(0.3f, 0.3f, 1.0f, 0.0f);
	primitive.radius = 0.2f;
	primitive.material = MATERIAL_METAL;

	list.push_back(primitive);

	primitive.position = createVector4<cl_float4, cl_float>(-0.5f, 0.0f, 1.3f, 1.0f);
	primitive.color = createVector4<cl_float4, cl_float>(0.0f, 0.3f, 0.3f, 0.0f);
	primitive.radius = 0.2f;
	primitive.material = MATERIAL_LAMBERTIAN;

	list.push_back(primitive);

	primitive.position = createVector4<cl_float4, cl_float>(0.2f, -0.0f, -1.0f, 1.0f);
	primitive.color = createVector4<cl_float4, cl_float>(0.3f, 0.3f, 0.3f, 0.0f);
	primitive.radius = 0.2f;
	primitive.material = MATERIAL_LAMBERTIAN;

	list.push_back(primitive);

	primitive.position = createVector4<cl_float4, cl_float>(-0.2f, -0.0f, -1.0f, 1.0f);
	primitive.color = createVector4<cl_float4, cl_float>(0.3f, 0.3f, 0.3f, 0.0f);
	primitive.radius = 0.2f;
	primitive.material = MATERIAL_LAMBERTIAN;

	list.push_back(primitive);

	primitive.position = createVector4<cl_float4, cl_float>(0.0f, -100.5f, -1.0f, 1.0f);
	primitive.color = createVector4<cl_float4, cl_float>(0.8f, 0.8f, 0.0f, 0.0f);
	primitive.radius = 100.0f;
	primitive.material = MATERIAL_METAL;

	list.push_back(primitive);

	return list;
}

Mat4 createMat4(glm::vec4 x_axis, glm::vec4 y_axis, glm::vec4 z_axis, glm::vec4 w_axis) {
	Mat4 mat;

	mat.x_axis = createVector4<cl_float4, cl_float>(x_axis.x, x_axis.y, x_axis.z, x_axis.w);
	mat.y_axis = createVector4<cl_float4, cl_float>(y_axis.x, y_axis.y, y_axis.z, y_axis.w);
	mat.z_axis = createVector4<cl_float4, cl_float>(z_axis.x, z_axis.y, z_axis.z, z_axis.w);
	mat.w_axis = createVector4<cl_float4, cl_float>(w_axis.x, w_axis.y, w_axis.z, w_axis.w);

	return mat;
}

Camera createCamera(cl_float fov) {
	Camera cam;

	glm::mat4 m = glm::inverse(glm::lookAt(
			glm::vec3(0.5, 0.2, -0.2),
			glm::vec3(0.0, 0.0, -1.0),
			glm::vec3(0.0, 1.0, 0.0)
	));

	cam.lookAt = createMat4(m[0], m[1], m[2], m[3]);
	cam.fov = fov;
	cam.width = WINDOW_WIDTH;
	cam.height = WINDOW_HEIGHT;

	return cam;

}


