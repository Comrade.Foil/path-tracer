//
// Created by shapito on 11/9/18.
//

#ifndef PATH_TRACING_PATH_TRACER_H
#define PATH_TRACING_PATH_TRACER_H

#define GLM_ENABLE_EXPERIMENTAL

#include "SDL2/SDL.h"
#include "cl-utils/cl_utils.h"

#include <glm/glm.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/detail/type_mat.hpp>
#include <glm/detail/type_mat4x4.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include <cstdlib>
#include <random>

#define WINDOW_WIDTH 1200
#define WINDOW_HEIGHT 600

#define UP 126
#define DOWN 125
#define RIGHT 124
#define LEFT 123

#define MATERIAL_LAMBERTIAN 10
#define MATERIAL_METAL 11

#define float_t  cl_float

typedef struct _Primitive {
	cl_float4	position;
	cl_float4	color;
	cl_float	radius;
	cl_int		material;
} Primitive;

typedef struct _State {
	cl_int width;
	cl_int height;
	cl_float time;
} State;

typedef struct _Mat4 {
	cl_float4 x_axis;
	cl_float4 y_axis;
	cl_float4 z_axis;
	cl_float4 w_axis;
} Mat4;

typedef struct _Camera {
	Mat4 lookAt;
	cl_float fov;
	cl_int width;
	cl_int height;
} Camera;

#endif //PATH_TRACING_PATH_TRACER_H
