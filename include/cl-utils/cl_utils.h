//
// Created by shapito on 11/9/18.
//

#ifndef PATH_TRACING_CL_UTILS_H
#define PATH_TRACING_CL_UTILS_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <string>
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.hpp>
#endif

#define ALIGN(x) __attribute__ ((aligned(x)))
#pragma OPENCL EXTENSION cl_khr_fp64 : enable

template <typename V, typename C>
V createVector4(C x, C y, C z, C w) {
	V t;

	t.x = x;
	t.y = y;
	t.z = z;
	t.w = w;

	return t;
}

template <typename V, typename C>
V createVector3(C x, C y, C z) {
	V t;

	t.x = x;
	t.y = y;
	t.z = z;

	return t;
}

template <typename V, typename C>
V createVector2(C x, C y, C z, C w) {
	V t;

	t.x = x;
	t.y = y;

	return t;
}

#endif //PATH_TRACING_CL_UTILS_H