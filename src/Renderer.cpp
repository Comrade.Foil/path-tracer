//
// Created by stefan on 11/19/18.
//

#include "Renderer.h"

Renderer::Renderer(Window *window) {
	this->window = window;
	errorCode = initRenderer();
	handleError();
}

Renderer::~Renderer() {
	destroy();
}

void Renderer::update(BitMap *bitMap) {
	SDL_UpdateTexture(
			frameBuffer,
			NULL,
			bitMap->data,
			bitMap->getWidth() * sizeof(Uint32)
	);
}

void Renderer::destroy() {
	if (errorCode == 0 && (frameBuffer != nullptr) && (renderer != nullptr)) {
		SDL_DestroyTexture(frameBuffer);
		SDL_DestroyRenderer(renderer);
	}
}

int Renderer::initRenderer() {
	this->renderer = SDL_CreateRenderer(window->getWindow(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (renderer == nullptr) {
		return 1;
	}
	frameBuffer = SDL_CreateTexture(renderer,
									SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_STREAMING, WINDOW_WIDTH, WINDOW_HEIGHT);
	if (frameBuffer == nullptr) {
		return 2;
	}

	return 0;
}

void Renderer::draw() {
	//First clear the renderer
	SDL_RenderClear(renderer);

	//Draw the texture
	SDL_RenderCopyEx(renderer, frameBuffer, NULL, NULL, 0, NULL, SDL_FLIP_VERTICAL);
	//SDL_RenderCopy(renderer, frameBuffer, NULL, NULL);

	//Update the screen
	SDL_RenderPresent(renderer);
}

void Renderer::handleError() {
	if (errorCode == 1) {
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
	} else if (errorCode == 2) {
		std::cout << "SDL_CreateTexture Error: " << SDL_GetError() << std::endl;
	}

}
