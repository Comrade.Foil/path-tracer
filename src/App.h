//
// Created by stefan on 11/19/18.
//

#ifndef PATH_TRACING_APP_H
#define PATH_TRACING_APP_H

#include "Renderer.h"
#include "Window.h"


class App {
public:
	Renderer *renderer = nullptr;
	Window *window = nullptr;

	App(const char* name, int x, int y, int width, int height);
	~App();

	bool isClosed();

private:


	void init();
	void destroy();
};


#endif //PATH_TRACING_APP_H
