//
// Created by stefan on 11/19/18.
//

#include "App.h"

App::App(const char* name, int x, int y, int width, int height) {
	window = new Window(name, x, y, width, height);
	renderer = new Renderer(window);
}

App::~App() {
	destroy();
}

bool App::isClosed() {
	return window->closed;
}

void App::init() {
	// init opencl and stuff here
}

void App::destroy() {
	delete window;
	delete renderer;
}
