//
// Created by stefan on 1/12/19.
//

#include "CLManager.h"

CLManager::CLManager() {
	initialize();

	this->err = outputDeviceInfo();
	checkError("Outputting device info", __FILE__, __LINE__);
}

void CLManager::initialize() {
	int	i;

	cl_device_id     device_id;
	cl_context       context;
	cl_command_queue commandQueue;

	cl_uint numPlatforms;

	this->err = clGetPlatformIDs(0, NULL, &numPlatforms);
	checkError("Finding platforms", __FILE__, __LINE__);
	if (numPlatforms == 0)
	{
		printf("Found 0 platforms!\n");
	}

	// Get all platforms
	cl_platform_id Platform[numPlatforms];
	this->err = clGetPlatformIDs(numPlatforms, Platform, NULL);
	checkError("Getting platforms", __FILE__, __LINE__);

	// Secure a GPU
	for (i = 0; i < numPlatforms; i++) {
		this->err = clGetDeviceIDs(Platform[i], DEVICE, 1, &device_id, NULL);
		if (this->err == CL_SUCCESS) {
			break;
		}
	}

	if (device_id == NULL)
		checkError("Finding a device", __FILE__, __LINE__);

	context = clCreateContext(0, 1, &device_id, NULL, NULL, &this->err);
	checkError("Creating context", __FILE__, __LINE__);

	commandQueue = clCreateCommandQueue(context, device_id, 0, &this->err);
	checkError("Creating command queue", __FILE__, __LINE__);

	this->context = context;
	this->commandQueue = commandQueue;
	this->deviceId = device_id;
}

cl_program CLManager::createProgram(const char *path) {
	std::string source;
	std::ifstream file(path);

	if (!file){
		std::cout << "\nCannot open OpenCL file!" << std::endl << "Exiting..." << std::endl;
		return NULL;
	}

	while (!file.eof()){
		char line[256];
		file.getline(line, 255);
		source += line;
	}

	const size_t len = source.length();
	const char* kernel_source = source.c_str();

	cl_program program = clCreateProgramWithSource(
			context,
			1,
			(const char **)&kernel_source,
			(const size_t *)&len,
			&this->err
	);

	if (this->err != CL_SUCCESS) {
		checkError("Creating program", __FILE__, __LINE__);
		return nullptr;
	} else {
		return program;
	}
}

cl_kernel CLManager::createKernel(cl_program program, const char *kernelName) {

	if (clBuildProgram(program, 0, NULL, NULL, NULL, NULL) != CL_SUCCESS)
	{
		size_t logSize;
		this->err = clGetProgramBuildInfo(program, deviceId,
										   CL_PROGRAM_BUILD_LOG, 0, NULL, &logSize);
		char *log = new char[logSize];
		this->err  = clGetProgramBuildInfo(program, deviceId,
									   CL_PROGRAM_BUILD_LOG, logSize, log, NULL);
		checkError("Building program", __FILE__, __LINE__);

		std::cout << log << std::endl;
	}

	cl_kernel kernel = clCreateKernel(program, kernelName, &this->err);
	checkError("Creating kernel", __FILE__, __LINE__);

	return kernel;
}

int CLManager::outputDeviceInfo() {
	cl_device_type device_type;         // Parameter defining the type of the compute device
	cl_uint comp_units;                 // the max number of compute units on a device
	cl_char vendor_name[1024] = {0};    // string to hold vendor name for compute device
	cl_char device_name[1024] = {0};    // string to hold name of compute device

	err = clGetDeviceInfo(deviceId, CL_DEVICE_NAME, sizeof(device_name), &device_name, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to access device name!\n");

		return EXIT_FAILURE;
	}
	printf(" \n Device is  %s ",device_name);

	err = clGetDeviceInfo(deviceId, CL_DEVICE_TYPE, sizeof(device_type), &device_type, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to access device type information!\n");
		return EXIT_FAILURE;
	}
	if(device_type  == CL_DEVICE_TYPE_GPU)
		printf(" GPU from ");

	else if (device_type == CL_DEVICE_TYPE_CPU)
		printf("\n CPU from ");

	else
		printf("\n non  CPU or GPU processor from ");

	err = clGetDeviceInfo(deviceId, CL_DEVICE_VENDOR, sizeof(vendor_name), &vendor_name, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to access device vendor name!\n");
		return EXIT_FAILURE;
	}
	printf(" %s ",vendor_name);

	err = clGetDeviceInfo(deviceId, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &comp_units, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to access device number of compute units !\n");
		return EXIT_FAILURE;
	}
	printf(" with a max of %d compute units \n",comp_units);

	return CL_SUCCESS;

}

void CLManager::checkError(const char *operation, char *filename, int line) {
	if (this->err != CL_SUCCESS) {
		std::cout << "Error during operation " << operation << std::endl;
		std::cout << "in " << filename << " on line " << line << std::endl;
		std::cout << "Error code was " << getErrorMessage(err) << err << " : "<< std::endl;
		exit(EXIT_FAILURE);
	}
}

const std::string CLManager::getErrorMessage(cl_int code) {
	switch (code) {
		case CL_SUCCESS:
			return (char*)"CL_SUCCESS";
		case CL_DEVICE_NOT_FOUND:
			return (char*)"CL_DEVICE_NOT_FOUND";
		case CL_DEVICE_NOT_AVAILABLE:
			return (char*)"CL_DEVICE_NOT_AVAILABLE";
		case CL_COMPILER_NOT_AVAILABLE:
			return (char*)"CL_COMPILER_NOT_AVAILABLE";
		case CL_MEM_OBJECT_ALLOCATION_FAILURE:
			return (char*)"CL_MEM_OBJECT_ALLOCATION_FAILURE";
		case CL_OUT_OF_RESOURCES:
			return (char*)"CL_OUT_OF_RESOURCES";
		case CL_OUT_OF_HOST_MEMORY:
			return (char*)"CL_OUT_OF_HOST_MEMORY";
		case CL_PROFILING_INFO_NOT_AVAILABLE:
			return (char*)"CL_PROFILING_INFO_NOT_AVAILABLE";
		case CL_MEM_COPY_OVERLAP:
			return (char*)"CL_MEM_COPY_OVERLAP";
		case CL_IMAGE_FORMAT_MISMATCH:
			return (char*)"CL_IMAGE_FORMAT_MISMATCH";
		case CL_IMAGE_FORMAT_NOT_SUPPORTED:
			return (char*)"CL_IMAGE_FORMAT_NOT_SUPPORTED";
		case CL_BUILD_PROGRAM_FAILURE:
			return (char*)"CL_BUILD_PROGRAM_FAILURE";
		case CL_MAP_FAILURE:
			return (char*)"CL_MAP_FAILURE";
		case CL_MISALIGNED_SUB_BUFFER_OFFSET:
			return (char*)"CL_MISALIGNED_SUB_BUFFER_OFFSET";
		case CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST:
			return (char*)"CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
		case CL_INVALID_VALUE:
			return (char*)"CL_INVALID_VALUE";
		case CL_INVALID_DEVICE_TYPE:
			return (char*)"CL_INVALID_DEVICE_TYPE";
		case CL_INVALID_PLATFORM:
			return (char*)"CL_INVALID_PLATFORM";
		case CL_INVALID_DEVICE:
			return (char*)"CL_INVALID_DEVICE";
		case CL_INVALID_CONTEXT:
			return (char*)"CL_INVALID_CONTEXT";
		case CL_INVALID_QUEUE_PROPERTIES:
			return (char*)"CL_INVALID_QUEUE_PROPERTIES";
		case CL_INVALID_COMMAND_QUEUE:
			return (char*)"CL_INVALID_COMMAND_QUEUE";
		case CL_INVALID_HOST_PTR:
			return (char*)"CL_INVALID_HOST_PTR";
		case CL_INVALID_MEM_OBJECT:
			return (char*)"CL_INVALID_MEM_OBJECT";
		case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:
			return (char*)"CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
		case CL_INVALID_IMAGE_SIZE:
			return (char*)"CL_INVALID_IMAGE_SIZE";
		case CL_INVALID_SAMPLER:
			return (char*)"CL_INVALID_SAMPLER";
		case CL_INVALID_BINARY:
			return (char*)"CL_INVALID_BINARY";
		case CL_INVALID_BUILD_OPTIONS:
			return (char*)"CL_INVALID_BUILD_OPTIONS";
		case CL_INVALID_PROGRAM:
			return (char*)"CL_INVALID_PROGRAM";
		case CL_INVALID_PROGRAM_EXECUTABLE:
			return (char*)"CL_INVALID_PROGRAM_EXECUTABLE";
		case CL_INVALID_KERNEL_NAME:
			return (char*)"CL_INVALID_KERNEL_NAME";
		case CL_INVALID_KERNEL_DEFINITION:
			return (char*)"CL_INVALID_KERNEL_DEFINITION";
		case CL_INVALID_KERNEL:
			return (char*)"CL_INVALID_KERNEL";
		case CL_INVALID_ARG_INDEX:
			return (char*)"CL_INVALID_ARG_INDEX";
		case CL_INVALID_ARG_VALUE:
			return (char*)"CL_INVALID_ARG_VALUE";
		case CL_INVALID_ARG_SIZE:
			return (char*)"CL_INVALID_ARG_SIZE";
		case CL_INVALID_KERNEL_ARGS:
			return (char*)"CL_INVALID_KERNEL_ARGS";
		case CL_INVALID_WORK_DIMENSION:
			return (char*)"CL_INVALID_WORK_DIMENSION";
		case CL_INVALID_WORK_GROUP_SIZE:
			return (char*)"CL_INVALID_WORK_GROUP_SIZE";
		case CL_INVALID_WORK_ITEM_SIZE:
			return (char*)"CL_INVALID_WORK_ITEM_SIZE";
		case CL_INVALID_GLOBAL_OFFSET:
			return (char*)"CL_INVALID_GLOBAL_OFFSET";
		case CL_INVALID_EVENT_WAIT_LIST:
			return (char*)"CL_INVALID_EVENT_WAIT_LIST";
		case CL_INVALID_EVENT:
			return (char*)"CL_INVALID_EVENT";
		case CL_INVALID_OPERATION:
			return (char*)"CL_INVALID_OPERATION";
		case CL_INVALID_GL_OBJECT:
			return (char*)"CL_INVALID_GL_OBJECT";
		case CL_INVALID_BUFFER_SIZE:
			return (char*)"CL_INVALID_BUFFER_SIZE";
		case CL_INVALID_MIP_LEVEL:
			return (char*)"CL_INVALID_MIP_LEVEL";
		case CL_INVALID_GLOBAL_WORK_SIZE:
			return (char*)"CL_INVALID_GLOBAL_WORK_SIZE";
		case CL_INVALID_PROPERTY:
			return (char*)"CL_INVALID_PROPERTY";

		default:
			return (char*)"UNKNOWN ERROR";
	}
}

CLManager::~CLManager() {

	if (commandQueue != nullptr)
	{
		clReleaseCommandQueue(commandQueue);
		commandQueue = nullptr;
	}

	if (context != nullptr)
	{
		clReleaseContext(context);
		context = nullptr;
	}
}
