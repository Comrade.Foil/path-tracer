//
// Created by stefan on 1/12/19.
//

#ifndef PATH_TRACING_CLMANAGER_H
#define PATH_TRACING_CLMANAGER_H

#include "path_tracer.h"

# ifndef DEVICE
	# define DEVICE CL_DEVICE_TYPE_GPU
# endif

class CLManager
{
public:
	CLManager();
	~CLManager();

	void initialize();

	cl_program createProgram(const char *paths);
	cl_kernel createKernel(cl_program program, const char *kernelName);
	int outputDeviceInfo();

	//static void checkError(cl_int result, const std::string& message);

	void checkError(const char *operation, char *filename, int line);
	static const std::string getErrorMessage(cl_int code);

	cl_platform_id platformId = nullptr;
	cl_device_id deviceId = nullptr;
	cl_context context = nullptr;
	cl_command_queue commandQueue = nullptr;
	cl_program printSizesProgram = nullptr;
	cl_kernel printSizesKernel = nullptr;
	cl_int err;
};

#endif //PATH_TRACING_CLMANAGER_H
