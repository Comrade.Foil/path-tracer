//
// Created by shapito on 11/11/18.
//

#ifndef PATH_TRACING_FRAMEBUFFER_H
#define PATH_TRACING_FRAMEBUFFER_H

#include "path_tracer.h"

class BitMap {
public:
    Uint32* data;

    BitMap(cl_uint width, cl_uint height);
    ~BitMap();

    void setPixelColor(cl_uint x, cl_uint y, Uint32 color);
    void clear();

    cl_uint getWidth() const;
    cl_uint getHeight() const;
	cl_uint getPixelsNum() const;

private:
    cl_uint width, height;



};


#endif //PATH_TRACING_FRAMEBUFFER_H
