constant int MAX_BOUNCES = 10;
constant int PASSES_COUNT = 20;
constant int MATERIAL_LAMBERTIAN = 10;
constant int MATERIAL_METAL = 11;

typedef struct State
{
	int   width;
	int   height;
	float time;
} State;

typedef struct 
{
    float4 x_axis;
    float4 y_axis;
    float4 z_axis;
    float4 w_axis;
} Mat4;

typedef struct
{
    float4 origin;
    float4 direction;
} Ray;

typedef struct
{
    float4 position;
    float4 color;
    float  radius;
    int    material;
} Sphere;

typedef struct
{
    float4 position;
    float4 normal;
    float4 color;
    int material;
    float  t;
} Record;

typedef struct
{
    Mat4    lookAt;
    float   fov;
    int     width;
    int     height;
} Camera;

Mat4 constructMat4(float4 x_a, float4 y_a, float4 z_a, float4 w_a) 
{
    Mat4 mat;

    mat.x_axis = x_a;
    mat.y_axis = y_a;
    mat.z_axis = z_a;
    mat.w_axis = w_a;

    return mat;
}

Ray constructRay(float4 origin, float4 direction)
{
    Ray r;

    r.origin = origin;
    r.direction = direction;

    return r;
}

int int3ToRGB(int r, int g, int b)
{
    r = clamp(r, 0, 255);
    g = clamp(g, 0, 255);
    b = clamp(b, 0, 255);

    return ((r << 16) | (g << 8) | b);
}

int float3ToRGB(float r, float g, float b)
{
    int _r = (int)(r * 255.9f);
    int _g = (int)(g * 255.9f);
    int _b = (int)(b * 255.9f);

    return int3ToRGB(_r, _g, _b);
}

uint randomInt(global uint2* state)
{
	enum { A = 4294883355U };
	uint x = (*state).x;
	uint c = (*state).y;
	uint res = x ^ c;
	uint hi = mul_hi(x, A);
	x = x * A + c;
	c = hi + (x < c);
	*state = (uint2)(x, c);

	return res;
}

float4 Mat4xFloat4(Mat4 m, float4 v) 
{
    float4 r;
    float4 mx;
    float4 my;
    float4 mz;
    float4 mw;

    mx = (float4)(m.x_axis.x, m.y_axis.x, m.z_axis.x, m.w_axis.x);
    my = (float4)(m.x_axis.y, m.y_axis.y, m.z_axis.y, m.w_axis.y);
    mz = (float4)(m.x_axis.z, m.y_axis.z, m.z_axis.z, m.w_axis.z);
    mw = (float4)(m.x_axis.w, m.y_axis.w, m.z_axis.w, m.w_axis.w);

    r = (float4)(dot(v, mx), dot(v, my), dot(v, mz), dot(v, mw));

    return r;
}

float randomFloat(global uint2* state)
{
	return (float)randomInt(state) / (float)0xFFFFFFFF;
}

float4 reflect(float4 incident, float4 normal) {
    return incident - 2 * dot(incident, normal) * normal;
}

float4 randomInUnitSphere(global uint2* state)
{
    float3 v = (float3)(randomFloat(state), randomFloat(state), randomFloat(state));
    float3 u = (float3)(1.0, 1.0, 1.0);

    do {
        v = 2.0f * (float3)(randomFloat(state), randomFloat(state), randomFloat(state)) - u;
    } while (length(v)*length(v) >= 1.0);

    return (float4)(v.xyz, 0.0f);
}

Ray scatterLambertian(Ray* incident, Record* hitRecord, global uint2* seed) 
{
    float4 target = hitRecord->position + hitRecord->normal + randomInUnitSphere(seed);
    Ray reflected = constructRay(hitRecord->position, normalize(target - hitRecord->position));

    return reflected;
}

Ray scatterMetallic(Ray* incident, Record* hitRecord, global uint2* seed) 
{
    float4 target = reflect(incident->direction, hitRecord->normal);
    Ray reflected = constructRay(hitRecord->position, normalize(target - hitRecord->position));

    return reflected;
}

Ray scatter(Ray* incident, Record* hitRecord, global uint2* seed)
{
    int material = hitRecord->material;

    if (material == MATERIAL_LAMBERTIAN) {
        return scatterLambertian(incident, hitRecord, seed);
    } else if (material == MATERIAL_METAL) {
        return scatterMetallic(incident, hitRecord, seed);
    }

    return constructRay((float4)(0.0, 0.0, 0.0, 1.0), (float4)(0.0, 1.0, 0.0, 0.0));
}

Ray createCameraRay(global Camera* camera, int x, int y)
{
    float fovFactor = tan((camera->fov * M_PI/180)/2);
    float aspectRatio = camera->width/camera->height;

    float _x = (2 * (((float)x + 0.5) / camera->width) - 1) * fovFactor * aspectRatio;
    float _y = (2 * (((float)y + 0.5) / camera->height) - 1) * fovFactor;

    float4 origin = (float4)(0.0, 0.0, 0.0, 1.0);
    float4 direction = (float4)(_x, _y, -1.0, 0.0);

    origin = Mat4xFloat4(camera->lookAt, origin);
    direction = Mat4xFloat4(camera->lookAt, direction);

    return constructRay(origin, normalize(direction));
}

float4 pointAtParameter(const Ray* r, float t)
{
    return (r->origin + (r->direction*t));
}

bool intersectSphere(const Sphere* s, const Ray* r, float t_min, float t_max, Record* rec)
{
    float4 oc  = (*r).origin - s->position;
    float a = dot((*r).direction, (*r).direction);
    float b = 2.0 * dot(oc, (*r).direction);
    float c = dot(oc, oc) - s->radius*s->radius;
    float d = b*b - 4*a*c;

    if (d > 0.0)
    {
        float temp = (-b - sqrt(d)) / (2.0*a);
        if (temp < t_max && temp > t_min)
        {
            rec->t          = temp;
            rec->position   = pointAtParameter(r, temp);
            rec->normal     = normalize(rec->position - s->position);
            rec->color      = s->color;
            rec->material   = s->material;

            return true;
        }

        temp = (-b + sqrt(d)) / (2.0*a);
        if (temp < t_max && temp > t_min)
        {
            rec->t          = temp;
            rec->position   = pointAtParameter(r, temp);
            rec->normal     = normalize(rec->position - s->position);
            rec->color      = s->color;
            rec->material   = s->material;

            return true;
        }
    }

    return false;
}

bool traceList(global Sphere* list, int num, const Ray* ray, Record* rec)
{
    float t_min = 0.0001;
    float closest = FLT_MAX;
    bool result = false;

    for (int i=0; i < num; i++)
    {
        Sphere current = list[i];
        if (intersectSphere(&current, ray, t_min, closest, rec))
        {
            closest = rec->t;
            result = true;
        }
    }

    return result;
}

float4 fireRay(__global Sphere* spheres, int num, Ray* ray, global uint2* seed, float3* back)
{
    Record rec;
    bool hitOccurred = true;
    int hits = 0;
    float4 finalColor = (float4)(1.0f, 1.0f, 1.0f, 1.0f);

    for (; (hits < MAX_BOUNCES) && hitOccurred; hits++)
    {
        hitOccurred = traceList(spheres, num, ray, &rec);

        if (hitOccurred)
        {
            Ray reflected = scatter(ray, &rec, seed);

            ray->origin = reflected.origin;
            ray->direction = reflected.direction;

            finalColor *= rec.color;
        }
        else
        {
            float4 u = normalize(ray->direction);
            float t = 0.5f * (u.y + 1.0f);

            finalColor *= (1.0f-t)*(float4)(1.0f, 1.0f, 1.0f, 0.0f)+t*(float4)(back->x, back->y, back->z, 0.0f);
        }
    }

    return finalColor;
}

__kernel void path_tracing(
    global int* output,
    global State* s,
    global Camera* camera,
    global uint2* seeds,
    global Sphere* spheres,
    int num)
{
    const int work_item_id = get_global_id(0);

    int x = work_item_id % s->width;
    int y = work_item_id / s->width;

    float2  uv = (float2)((float)x / (float)s->width, (float)y / (float)s->height);
    float4 clr = (float4)(0.0f, 0.0f, 0.0f, 0.0f);

    float3 background = (float3)(
                    0.5+0.5*cos(s->time+uv.x+0.0),
                    0.5+0.5*cos(s->time+uv.y+2.0),
                    0.5+0.5*cos(s->time+uv.x+4.0));

    float3 back = (float3)(0.5f, 0.7f, 1.0f);

    for (int i=0; i < PASSES_COUNT; i++)
    {
        Ray cameraRay = createCameraRay(camera, x, y);
        clr += sqrt(fireRay(spheres, num, &cameraRay, &seeds[work_item_id], &back));
    }

    Ray what = createCameraRay(camera, x, y);

    clr /= PASSES_COUNT;

    output[work_item_id] = float3ToRGB(clr.x, clr.y, clr.z);
}