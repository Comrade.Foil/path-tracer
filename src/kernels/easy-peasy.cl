int createRGB(int r, int g, int b)
{
    r = clamp(r, 0, 255);
    g = clamp(g, 0, 255);
    b = clamp(b, 0, 255);

    return ((r << 16) | (g << 8) | b);
}

__kernel void render_kernel(__global int* output, int width, int height, float time)
{
    const int work_item_id = get_global_id(0);
    int color;

    int x = work_item_id % width;
    int y = work_item_id / width;

    float2  uv = (float2)((float)x / (float)width, (float)y / (float)height);

    float3 col = (float3)(
                    (0.5+0.5*cos(time+uv.x+0.0)) * 255.9f,
                    (0.5+0.5*cos(time+uv.y+2.0)) * 255.9f,
                    (0.5+0.5*cos(time+uv.x+4.0)) * 255.9f);

    color = createRGB((int)(col.x), (int)(col.y), (int)(col.z));

    output[work_item_id] = color;
}