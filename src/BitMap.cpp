//
// Created by shapito on 11/11/18.
//

#include "BitMap.h"

BitMap::BitMap(cl_uint width, cl_uint height)
{
    this->width = width;
    this->height = height;

    this->data = new Uint32[width*height];
}

BitMap::~BitMap()
{
    delete [] data;
}

void BitMap::setPixelColor(cl_uint x, cl_uint y, Uint32 color)
{
    this->data[(y * this->width) + x] = color;
}

cl_uint BitMap::getWidth() const {
    return width;
}

cl_uint BitMap::getHeight() const {
    return height;
}

cl_uint BitMap::getPixelsNum() const {
    return width * height;
}

void BitMap::clear() {
    memset(this->data, 255, this->width * this->height * sizeof(Uint32));
}
