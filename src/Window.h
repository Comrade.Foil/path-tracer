//
// Created by stefan on 11/18/18.
//

#ifndef PATH_TRACING_WINDOW_H
#define PATH_TRACING_WINDOW_H

#include "path_tracer.h"

class Window {
public:
	Window(const char* name, int x, int y, int width, int height);
	~Window();

	bool closed = false;

	void readKeyboardInput();
	SDL_Window *getWindow();
	void handleError();

private:
	SDL_Window *window = nullptr;
	SDL_Event event;

	int errorCode;

	int initWindow(const char* name, int x, int y, int width, int height);
	void destroy();

};


#endif //PATH_TRACING_WINDOW_H
