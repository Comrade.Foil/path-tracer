//
// Created by stefan on 11/19/18.
//

#ifndef PATH_TRACING_RENDERER_H
#define PATH_TRACING_RENDERER_H

#include "path_tracer.h"
#include "Window.h"
#include "BitMap.h"

class Renderer {
public:
	Window *window = nullptr;
	SDL_Renderer *renderer = nullptr;
	SDL_Texture *frameBuffer = nullptr;

	int errorCode;

	Renderer(Window *window);
	~Renderer();

	void update(BitMap *bitMap);
	void draw();
	void handleError();

private:
	int initRenderer();
	void destroy();
};


#endif //PATH_TRACING_RENDERER_H
