//
// Created by stefan on 11/18/18.
//

#include "Window.h"

Window::Window(const char* name, int x, int y, int width, int height) {
	errorCode = initWindow(name, x, y, width, height);
	handleError();
}

Window::~Window() {
	if (window != nullptr) {
		destroy();
		SDL_Quit();
	}
}


SDL_Window *Window::getWindow() {
	return window;
}

int Window::initWindow(const char* name, int x, int y, int width, int height) {
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {

		return 1;
	}
	this->window = SDL_CreateWindow(name, x, y, width, height, SDL_WINDOW_SHOWN);
	if (this->window == nullptr) {

		return 2;
	}

	return 0;
}

void Window::readKeyboardInput() {
	SDL_PollEvent(&event);

	switch (event.type) {
		/* Keyboard event */
		/* Pass the event data onto PrintKeyInfo() */
		case SDL_KEYDOWN:
			if (event.key.keysym.sym == SDLK_ESCAPE) {
				closed = true;
				break;
			}
		case SDL_KEYUP:
			std::cout << "KEYUP" << event.type << std::endl;
			break;

			/* SDL_QUIT event (window close) */
		case SDL_QUIT:
			closed = true;
			break;

		default:
			break;
	}
}

void Window::destroy() {
	SDL_DestroyWindow(this->window);
}

void Window::handleError() {
	if (errorCode == 1) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
	} else if (errorCode == 2){
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
	}
}

